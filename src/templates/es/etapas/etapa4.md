Etapa sin apenas complicaciones técnicas ni pendientes especialmente reseñables. Destacar las bajadas de los kilómetros 24 y 27, con zonas con mucha piedra suelta. Por lo demás, etapa muy apta para ciclistas muy rodadores.

Destacar también el cruce del kilómetro 33´2 donde, en una encrucijada, debemos tomar a la derecha (un camino en subida con la entrada un tanto oculta) y no seguir de frente.
