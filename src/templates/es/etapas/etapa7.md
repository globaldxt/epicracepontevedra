Etapa donde hay que destacar la primera subida, con un tramo corto entorno al kilómetro 5 que llega a alcanzar un 19% de desnivel, aunque el firme es bueno.

En cuanto a las bajadas, destacar especialmente el tramo entre los kilómetros 14 y 15 (camino de hierba donde hay que buscar la trazada limpia) y las escaleras finales en el kilómetro 23.

Reseñar también un pequeño tramo con escalones separados, apenas 200m después del punto anterior, un acceso en subida al puente que cruza sobre el río.
