Las zonas técnicas de la etapa son principalmente por márgenes de ríos, en su mayor parte son perfectamente ciclables, aunque sí existe algún paso estrecho o puntos concretos donde el agua ha dejado piedras sueltas con las crecidas.

En cuanto a las pendientes de subida destacar dos tramos duros, el primero cerca del kilómetro 5, (que supera un 22% en una rampa de cemento de unos 150m) y el segundo un camino de grava en el kilómetro 19, que ronda el 20% de inclinación.
