Etapa en la que los tramos técnicos se salpican a lo largo de la ruta. Algunas de estas zonas son en subida, como algunas rampas con piedra suelta entorno a los kilómetros 6, 9 o 25, pero no son tramos largos.

Las partes técnicas en bajada son principalmente por caminos empedrados , destacando especialmente la preciosa bajada de la senda del kilómetro 18 y la del 37, que presentan algún punto con escalones sobre losa.
