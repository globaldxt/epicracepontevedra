Etapa donde priman las subidas sin gran pendiente y sin complicaciones técnicas. Las dos rampas con mayor inclinación son de apenas 200m y coinciden sobre asfalto.

Las zonas más técnicas, entre los kilómetros 36 y 44 son por los típicos caminos empedrados gallegos, en ligero descenso y sin piedra suelta.
